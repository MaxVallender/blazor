Microsoft recommended **.NET Core SDK 3.0** for Blazor.
Or **Visual Studio 2019 Version 16**. Fragezeichen 2017 doesn't do it.

I was a little in trouble to install Blazor under my Linux-Machine.
Had installed **Visual Studio Core** and **.NET Core** already.
At many manuals I found something link the next line, which should be done.

	$ dotnet new --install Microsoft.AspNetCore.Components.WebAssembly.Templates::3.2.0-preview2.20168.3

And **dotnet** always throws out **warning NU1603** and doesn't do anything. The funny thing is. This doesn't need to be done! It is already inside. :D

	$ dotnet new
	...
	Templates                                         Short Name               Language          Tags
	----------------------------------------------------------------------------------------------------------------------------------
	...
	Blazor Server App                                 blazorserver             [C#]              Web/Blazor
	Blazor WebAssembly App                            blazorwasm               [C#]              Web/Blazor/WebAssembly
	...

So there are also existing templates for a demo-project.

By the way, you can check out your installation with:

	$ dotnet --info

And get update **dotnet** by:

	$ dotnet new --update-apply
	
(Package manager under Linux seem to do this automatic.)

#		B l a z o r - W e b A s s e m b l y

So, to initialize a new Blazor-Project, you only need to instruct **dotnet** to do so:

	$ dotnet new blazorwasm -o BlazorWAsmDemo

Now the project can be started:

	$ cd BlazorWAsmDemo
	$ dotnet run
	info: Microsoft.Hosting.Lifetime[0]
	      Now listening on: http://localhost:5000
	info: Microsoft.Hosting.Lifetime[0]
	      Now listening on: https://localhost:5001
	info: Microsoft.Hosting.Lifetime[0]
	      Application started. Press Ctrl+C to shut down.
	info: Microsoft.Hosting.Lifetime[0]
	      Hosting environment: Production
	info: Microsoft.Hosting.Lifetime[0]
	      Content root path: /home/max/testNet/BlazorWAsmDemo
	...


Now you can access to it via a browser (like chromium).

	$ chromium http://localhost:5000/

To end the local server press **Ctrl+C** (like mentioned in the output).

#		CODE

Here is the basic of a **.razor** File.
For the button there is a class **btn btn-primary** used.

	@page "/Pagename"
	
	@Variable

	<button class="btn btn-primary" @onclick="Action">Text</button>

	@code {
		private int Variable;
		
		private void Action() {
			...
		}
	}

The HTML-Code and the C#-Code is separated from each other. Only by using a concrete value or function, there is the use of **@** inside the HTML-Code. Or if if-else, for, ... is used in the code for flow control.

#		B l a z o r - S e r v e r

FRAGEZEICHEN KOPIERT
So, to initialize a new Blazor-Project, you only need to instruct **dotnet** to do so:

	$ dotnet new blazorserver -o BlazorServerDemo

Amazing, it's the same webside. But with a different internal structure. Which we can build, the same way.

	$ cd BlazorServerDemo
	$ dotnet run
	info: Microsoft.Hosting.Lifetime[0]
	      Now listening on: https://localhost:5001
	info: Microsoft.Hosting.Lifetime[0]
	      Now listening on: http://localhost:5000
	info: Microsoft.Hosting.Lifetime[0]
	      Application started. Press Ctrl+C to shut down.
	info: Microsoft.Hosting.Lifetime[0]
	      Hosting environment: Development
	info: Microsoft.Hosting.Lifetime[0]
	      Content root path: /home/max/testNet/BlazorServerDemo
	
Now you can access to it via a browser (like chromium).

	$ chromium http://localhost:5000/

To end the local server press **Ctrl+C** (like mentioned in the output).

#		D i f f e r e n c e

**Meld** tells us that there are most files are the same. The html-Code is nearly identical. But because of the different levels of compilation, there must be a difference in the 

